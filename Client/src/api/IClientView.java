package api;

public interface IClientView {
	void setControler(IClientControler c);
	int getAnswer();
	void log(String message);
}
