package api;


public interface IClientControler {
	void setView(IClientView view);
	boolean checkConnection();
	boolean connect(String host, int port);
	void sendRequest(String cmd);
	void getResponse();
	void disconnect();
}
