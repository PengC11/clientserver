package impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import api.IClientControler;
import api.IClientView;

public class ClientControler_impl implements IClientControler{
	
	private IClientView clientView;
	private Socket clientSocket;
	private String localHost;
	private String serverHost;
	private int serverPort;
	
	private PrintWriter out;
	private BufferedReader in;
	private String response;
	private SessionThread sessionThread;
	
	@Override
	public void setView(IClientView view) {
		this.clientView = view;
	}
	
	@Override
	public boolean checkConnection() {
		return isConnected();
	}
	public boolean isConnected(){
		boolean result = clientSocket != null && !clientSocket.isClosed() && clientSocket.isConnected();
		return result;
	}
	
	@Override
	public boolean connect(String host, int port) {
		if(!isConnected()){
			this.serverHost = host;
			this.serverPort = port;
			try {
				openSocket();
			} catch (IOException e) {
				e.printStackTrace();
			}
			sessionThread = new SessionThread();
			sessionThread.start();
			return true;
		}
		return false;
	}
	private void openSocket() throws UnknownHostException, IOException {
		try {
			clientSocket = new Socket(serverHost, serverPort);
			localHost = clientSocket.getLocalAddress().getHostAddress();
		} catch (Exception e) {
			in = null;
			out = null;
			throw new IOException("connection error");
		}
		in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		out = new PrintWriter(clientSocket.getOutputStream(), true);
		out.println(localHost+" is connected");
		clientView.log("Connected to the server");
	}

	@Override
	public void sendRequest(String cmd) {
		if(cmd.equals("begin game")){
			if(isConnected()){
				out.println(cmd);
				out.flush();
			}else{
				clientView.log("connection error!");
			}
			
		}else if(cmd.equals("submit")){
			int answerClient=-1;
			try {
				answerClient = clientView.getAnswer();
			} catch (Exception e) {
				clientView.log("your answer is not a number!");
			}
			if(answerClient!=-1)
				if(isConnected()){
					out.println(cmd+answerClient);
					out.flush();
				}else{
					clientView.log("connection error!");
				}
			
		}
	}
		
	@Override
	public void getResponse() {
		try {
			response = in.readLine();
		} catch (IOException e) {
			
		}
		if(response.equals("game over")){
			clientView.log(response+"\r\n"+"\n"+"You have winned !\n");
			disconnect();
		}else if(response.equals("game begun")){
			clientView.log(response+"\r\n"+"please input your answer!");
		}else if(response.equals("greater")){
			clientView.log("Your input is greater than the answer");
		}else if(response.equals("less")){
			clientView.log("Your input is less than the answer");
		}
	}

	@Override
	public void disconnect() {
		if(isConnected())closeSocket();	
	}
	private void closeSocket() {
		if (clientSocket != null)
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (in != null)
			try {
				in.close();
				in = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (out != null) {
			out.close();
			out = null;
		}
	}
	
	class SessionThread extends Thread {
				
		@Override
		public void run() {
			while (isConnected()) {
				getResponse();
			}
			disconnect();
		}
	}
}
