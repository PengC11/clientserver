package api;


public interface IServerControler {
	static final int DEFAULT_PORT = 8051;
	void start();
	void end();
	void setServerGui(IServerView serverGui);
}
