package api;

import java.util.List;

public interface IServerView {
	void setListClients(List<String> client);
	void setHostAndPort(String host, int port);
	void setControler(IServerControler serverControler);
	void setAnswer(int answer);
	void log(String msg);
}
