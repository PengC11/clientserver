package impl;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

import api.IServerControler;
import api.IServerView;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.wb.swt.SWTResourceManager;

public class ServerGui_Impl implements IServerView{

	protected Shell shell;
	private Text txtanswer;
	private Text txtmessage;
	private Button btnStart;
	private Label lblHost;
	private Text txtHost;
	private Label lblPort;
	private Text txtport;
	private Label lblClients;	
	private IServerControler serverControler; 
	private org.eclipse.swt.widgets.List listClients;
	
	private static final String[] EMPTY_ARRAY = {};
	/**
	 * Launch the application.
	 * @param args
	 */
	@Override
	public void setListClients(final List<String> clients) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				if (clients == null || clients.isEmpty())
					listClients.setItems(EMPTY_ARRAY);
				else {
					String[] c = new String[clients.size()];
					for (int i = 0; i < c.length; i++)
						c[i] = clients.get(i);
					listClients.setItems(c);
				}
			}
		});
	}

	@Override
	public void setControler(IServerControler serverControler) {
		this.serverControler = serverControler;
		this.serverControler.setServerGui(this);
	}

	@Override
	public void setAnswer(int answer) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				txtanswer.setText(""+answer);
			}
		});
				
	}

	@Override
	public void log(String msg) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				txtmessage.setText(txtmessage.getText() + "\r\n" + msg);
				txtmessage.setTopIndex(txtmessage.getLineCount() - 1);
			}
		});
	}
	
	@Override
	public void setHostAndPort(String host, int port) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				txtHost.setText(host);
				txtport.setText(Integer.toString(port));
			}
		});
	}

	public static void main(String[] args) {
		try {
			ServerGui_Impl window = new ServerGui_Impl();
			window.setControler(new ServerControler_Impl());
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		
		shell.layout();
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				serverControler.end();
				shell.dispose();
			}
		});
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setSize(304, 342);
		shell.setText("Server");
		shell.setLayout(new GridLayout(2, false));
		
		Label lblNumber = new Label(shell, SWT.NONE);
		lblNumber.setText("Number");
		
		lblHost = new Label(shell, SWT.NONE);
		lblHost.setText("host :");
		
		txtanswer = new Text(shell, SWT.BORDER);
		txtanswer.setEnabled(false);
		txtanswer.setFont(SWTResourceManager.getFont("Segoe UI", 18, SWT.NORMAL));
		GridData gd_txtanswer = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 3);
		gd_txtanswer.widthHint = 141;
		gd_txtanswer.heightHint = 56;
		txtanswer.setLayoutData(gd_txtanswer);
		
		txtHost = new Text(shell, SWT.BORDER);
		txtHost.setEnabled(false);
		txtHost.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		lblPort = new Label(shell, SWT.NONE);
		lblPort.setText("port :");
		
		txtport = new Text(shell, SWT.BORDER);
		GridData gd_txtport = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtport.heightHint = 16;
		txtport.setLayoutData(gd_txtport);
		
		Label lblMessage = new Label(shell, SWT.NONE);
		lblMessage.setText("Message");
		
		lblClients = new Label(shell, SWT.NONE);
		lblClients.setText("clients");
		
		txtmessage = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		txtmessage.setEditable(false);
		GridData gd_txtmessage = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtmessage.widthHint = 116;
		gd_txtmessage.heightHint = 118;
		txtmessage.setLayoutData(gd_txtmessage);
		
		listClients = new org.eclipse.swt.widgets.List(shell, SWT.BORDER);
		GridData gd_listClients = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_listClients.heightHint = 139;
		gd_listClients.widthHint = 112;
		listClients.setLayoutData(gd_listClients);
		
		btnStart = new Button(shell, SWT.NONE);
		btnStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				serverControler.start();
			}
		});
		GridData gd_btnStart = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_btnStart.widthHint = 97;
		btnStart.setLayoutData(gd_btnStart);
		btnStart.setText("Start");
		new Label(shell, SWT.NONE);

	}

}
